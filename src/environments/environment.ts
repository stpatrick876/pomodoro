// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDfP0n4iyBM-jthcVOPtekZ2BOiUFuLsQs",
    authDomain: "pomodor-app-c7ba4.firebaseapp.com",
    databaseURL: "https://pomodor-app-c7ba4.firebaseio.com",
    projectId: "pomodor-app-c7ba4",
    storageBucket: "pomodor-app-c7ba4.appspot.com",
    messagingSenderId: "209973996477"
  }
};

// {
//   "rules": {
//   ".read": "auth != null",
//     ".write": "auth != null"
// }
// }
