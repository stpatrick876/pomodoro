import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {TimerService} from "./timer.service";
import {MdDialog, MdDialogRef} from "@angular/material";
import {WelcomeComponent} from "./auth/welcome.component";
import {AuthService} from "./auth.service";
import {LoginComponent} from "./auth/login/login.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit{
  showtasks = false;
  user: any;
   welcomeDialogRef: MdDialogRef<WelcomeComponent>;

  constructor(private timerSrv: TimerService, private authDialog: MdDialog, private _authSrv: AuthService, private elementRef: ElementRef, private render:  Renderer2){
    timerSrv.startTimerEventAnnounced$.subscribe(()=> {
      this.showtasks = false;
    });

    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state == false) {
        localStorage.removeItem('pomodoro-intro');
      }
    });

  }

  ngOnInit() {
    this._authSrv.user.subscribe(user => {
      this.user = user;
    });

    if (localStorage.getItem('pomodoro-intro') !== 'true'){
      this.welcomeDialogRef = this.authDialog.open(WelcomeComponent, {
        width: '80%',
        height: '80%',
        backdropClass: 'dialog-backdrop',
        disableClose: true
      });

      this.welcomeDialogRef.afterClosed().subscribe(() => {
        document.getElementsByTagName('body')[0].classList.remove('modal-open');
      });

      this.welcomeDialogRef.afterOpen().subscribe(() => {
        document.getElementsByTagName('body')[0].classList.add('modal-open');
        localStorage.setItem('pomodoro-intro', 'true');
      });
    }
  }

  logout() {
    this._authSrv.logout().then(res => {
      this.showtasks = false;
      this._authSrv.notifyLogInOutEvent(false);
    });
  }

  login(){
    this.render.removeClass(this.elementRef.nativeElement.querySelector('#loginBtn'), 'highlight-btn');

    const dialogRef = this.authDialog.open(LoginComponent, {
      width: '80%',
      height: '80%'
    });
    dialogRef.afterClosed().subscribe(() => {
      document.getElementsByTagName('body')[0].classList.remove('modal-open');
    });

    dialogRef.afterOpen().subscribe(() => {
      document.getElementsByTagName('body')[0].classList.add('modal-open');
    });
  }

  showTaskView() {
    if (this.user) {
      this.showtasks = true;
    } else {
      const el = this.elementRef.nativeElement.querySelector('#loginBtn');
      this.render.addClass(el, 'highlight-btn');

      setTimeout(() => {
        this.render.removeClass(el, 'highlight-btn');
      }, 30000);
    }
  }

}
