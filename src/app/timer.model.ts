
export enum State {
  COMPLETE,
  PAUSED,
  INPROGRESS,
  NOTSTARTED,
  ONBREAK
}

export enum TaskStatus {
  COMPLETED,
  INPROGRESS,
  CANCELED
}

export class Task {
  id: string;
  name: string;
  status: TaskStatus;
  sessionCt: number;
  lastActivity: number;
  user: string;
  isAnonymousUser: boolean;
  // create_ts: number;
  constructor() {
  }


  static buildTasks(dbList: any): Task[] {
   const tasks: Task[] = [];
    dbList.forEach((obj) => {
     const task = new Task();
      task.id = obj.$key;
      task.name = obj.task.name;
      task.status = obj.task.status;
      task.sessionCt = obj.task.sessionCt;
      task.lastActivity = obj.task.lastActivity;
      task.user = obj.task.user;
      if(typeof obj.task.isAnonymousUser !== 'undefined'){
        task.isAnonymousUser = obj.task.isAnonymousUser;
      }
      tasks.push(task);
    });

    return tasks;
  }



}

export interface Time {
   min: {
     value: number;
     displayValue: string;
   };
   sec: {
     value: number;
     displayValue: string;
   };
   setSeconds(seconds: number): void;
   setMinutes(minutes: number): void;
}
export class Timer {
  time: Time;
  sessionTime: number;
  state: State;
  task: Task;



  constructor() {

  }



}
