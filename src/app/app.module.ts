import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ClockComponent } from './clock/clock.component';
import { SettingsComponent } from './settings/settings.component';
import {TimerService} from "./timer.service";
import {FormsModule} from "@angular/forms";
import { TasksComponent } from './tasks/tasks.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MdButtonModule, MdDialogModule, MdExpansionModule, MdIconModule, MdListModule, MdMenuModule, MdStepperModule,
  MdTabsModule
} from "@angular/material";
import {AngularFireModule} from "angularfire2";
import {environment} from "../environments/environment";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuthModule} from "angularfire2/auth";
import { WelcomeComponent } from './auth/welcome.component';
import {AuthService} from "./auth.service";
import { LoginComponent } from './auth/login/login.component';
import { SessionCountChartComponent } from './tasks/session-count-chart/session-count-chart.component';
import { NumberInputComponent } from './number-input/number-input.component';
export const MD_MODULES = [  MdListModule, MdIconModule, MdButtonModule, MdMenuModule, MdTabsModule, MdDialogModule, MdStepperModule, MdExpansionModule];

@NgModule({
  declarations: [
    AppComponent,
    ClockComponent,
    SettingsComponent,
    TasksComponent,
    WelcomeComponent,
    LoginComponent,
    SessionCountChartComponent,
    NumberInputComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase, 'pomodoro-app'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ...MD_MODULES
  ],
  providers: [ TimerService, AuthService ],
  bootstrap: [AppComponent],
  entryComponents: [WelcomeComponent, LoginComponent]

})
export class AppModule { }
