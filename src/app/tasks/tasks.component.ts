import { Component, OnInit } from '@angular/core';
import {Task, TaskStatus} from "../timer.model";
import {TimerService} from "../timer.service";
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  taskStatus =  TaskStatus;
  tasksRef: FirebaseListObservable<any[]>;
  tasks: Task[];
  user: any;

  constructor(private timerSrv: TimerService, private afd: AngularFireDatabase, private _authSrv: AuthService) {}

  ngOnInit() {
    this.timerSrv.sessionEndEventAnnounced$.subscribe(task => {
      task.sessionCt = task.sessionCt + 1;
      this.tasksRef.update(task.id, { task: task });

    });

    this._authSrv.user.subscribe(user => {
      this.user = user;
      this.getTasks();
    });
  }
  private getTasks() {
    if (!this.user) return false;
    this.tasksRef = this.afd.list('/tasks', {
      query: {
        limitToLast: 50,
        orderByChild: 'task/user',
       equalTo: this.user.uid,
      }
    });
    this.tasksRef
      .subscribe(res => {
        this.tasks = Task.buildTasks(res);
      });
  }
  getIconClass(status: TaskStatus): string{
    let retClass: string;
    switch (status){
      case TaskStatus.COMPLETED:
        retClass = 'icon-complete';
        break;
      case TaskStatus.INPROGRESS:
        retClass = 'icon-active';
        break;
      case TaskStatus.CANCELED:
        retClass = 'icon-canceled';
        break;
    }
   return retClass;
  }

  addTask(name: string){
    if(!this.user) return false;
    const task: Task = new Task();
    task.name = name;
    task.status = TaskStatus.INPROGRESS;
    task.lastActivity = new Date().getTime();
    task.sessionCt = 0;
    task.user = this.user.uid;
    task.isAnonymousUser = this.user.isAnonymous;
    this.tasksRef.push({ task: task});
  }


  startTask(key: string, task: Task): void {
    task.status = TaskStatus.INPROGRESS;
    this.tasksRef.update(key, { task: task });
    this.timerSrv.notifyStartTimerEvent(task);
  }
  cancelTask(key: string, task: Task) {
    task.status = TaskStatus.CANCELED;
    this.tasksRef.update(key, { task: task });

  }
  completeTask(key: string, task: Task){
    task.status = TaskStatus.COMPLETED;
    this.tasksRef.update(key, { task: task });

  }

  removeTask(key: string){
    this.tasksRef.remove(key);
  }



}
