import {Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";
import {Task} from "../../timer.model";
import {isUndefined} from "util";
import {Observable} from "rxjs/Observable";

interface Data {
  label: string;
  count: number;
};

@Component({
  selector: 'app-session-count-chart',
  templateUrl: './session-count-chart.component.html',
  styleUrls: ['./session-count-chart.component.scss']
})
export class SessionCountChartComponent implements OnInit, OnChanges {
  private margin = {top: 20, right: 20, bottom: 30, left: 50};
  private width: number;
  private height: number;
  private radius: number;

  private arc: any;
  private labelArc: any;
  private pie: any;
  private color: any;
  private svg: any;
  private label: any;
  dataset: Data[];
  @Input() tasks: Task[];
  constructor(private el: ElementRef) {
    this.width = 360;
    this.height = 360;
    this.radius = Math.min(this.width, this.height) / 2;
    this.dataset = [];
  }

  ngOnInit() {
    this.initSvg();
  }


  private initSvg() {

    this.color = d3Scale.scaleOrdinal(d3Scale.schemeCategory20b);

    this.arc = d3Shape.arc()
      .innerRadius(0)
      .outerRadius(this.radius);

    this.pie = d3Shape.pie<any>()
      .value(d => d.count)
      .sort(null);

    this.label = d3Shape.arc()
      .outerRadius(this.radius - 40)
      .innerRadius(this.radius - 40);

  }

  private drawSvg() {
    const elem =  this.el.nativeElement.querySelector('#sessionPie');

    const svg = d3.select(elem)
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .append('g')
      .attr('transform', 'translate(' + (this.width / 2) + ',' + (this.height / 2) + ')');


    const path = svg.selectAll('path')
      .data(this.pie(this.dataset))
      .enter()
      .append('path')
      .attr('d', <any>this.arc)
      .attr('fill', (d, i) => this.color(((<any>d.data).label)))
      .append("text")
       // .attr("transform", function(d) { return "translate(" + this.label.centroid(d) + ")"; })
      .attr("dy", "0.35em")
      .text(function(d) { return d.data.label; });

  }

  buildDataSet(tasks: Observable<Task[]>) {
    console.log(tasks.count())
    tasks.map(task => {
    return  {
        label: task['name'],
       count: task['sessionCt']
      };
    }).subscribe(task => {
      console.log(task)
      this.dataset.push(task)
    }, error2 => {}, () => {
      this.drawSvg()

    });

    /*   tasks.forEach(task => {
         this.dataset.push({
           label: task.name,
           count: task.sessionCt
         });
       });*/


  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'tasks' && typeof changes['tasks'].currentValue !== 'undefined') {
        const tasks = changes['tasks'].currentValue;
          this.buildDataSet(Observable.from(tasks));
      }

    }
  }

}
