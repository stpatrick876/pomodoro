import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionCountChartComponent } from './session-count-chart.component';

describe('SessionCountChartComponent', () => {
  let component: SessionCountChartComponent;
  let fixture: ComponentFixture<SessionCountChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionCountChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionCountChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
