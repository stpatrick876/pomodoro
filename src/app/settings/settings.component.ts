import { Component, OnInit } from '@angular/core';
import {TimerService} from "../timer.service";
import {Timer} from "../timer.model";
import {AuthService} from "../auth.service";
import {isUndefined} from "util";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  sessionLength: number;
  breakLength: number;
  taskName: string;
  timer: Timer;

  constructor(private _timerSrv: TimerService, private _authSrv: AuthService) { }

  ngOnInit() {
    this.sessionLength = 25; // hardcoded
    this.breakLength = 10;
    this._timerSrv.timer$.subscribe((timer: Timer) => {
      this.timer = timer;
    });

    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state === false) {
       this.sessionLength = 25;
       this.timer = null;
      }
    });



  }

  onSessionLengthValChange(e) {
    this._timerSrv.updateSessionTime(e);
  }



}
