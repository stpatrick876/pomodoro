import {Injectable} from '@angular/core';
import {State, Task, Timer} from "./timer.model";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import 'rxjs'
import { Howl } from 'howler';
import {isBoolean, isUndefined} from "util";
import {isDefined} from "@angular/compiler/src/util";

@Injectable()
export class TimerService {
   timer: Timer;
  private start = 59;
  timer$ = new BehaviorSubject(new Timer());

  private progressEvent: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public progressEventAnnounced$ = this.progressEvent.asObservable();
  private startTimerEvent: Subject<Task> = new Subject<Task>();
  public startTimerEventAnnounced$ = this.startTimerEvent.asObservable();
  private sessionEndEvent: Subject<Task> = new Subject<Task>();
  public sessionEndEventAnnounced$ = this.sessionEndEvent.asObservable();

  private pause$: Subject<boolean> = new Subject<boolean>();
  constructor() { }

  init(task: Task) {
    const startTime = 25; // initial session time
    this.timer = new Timer();
    this.timer.sessionTime = startTime;
    this.timer.task = task;
    this.timer.time = {
      min: {
        value: 0,
        displayValue: '00'
      },
      sec: {
        value: 0,
        displayValue: '00'
      },
      setSeconds(seconds: number) {
        this.sec.value = seconds;

        if(seconds < 10){
          this.sec.displayValue = `0${seconds}`;
        }else {
          this.sec.displayValue = `${seconds}`;

        }
      },
      setMinutes(minutes: number) {
        this.min.value = minutes;

        if(minutes < 10){
          this.min.displayValue = `0${minutes}`;
        }else {
          this.min.displayValue = `${minutes}`;

        }
      }
    };
    this.notifyProgressEvent();

    this.timer.time.setMinutes(startTime);
    this.timer.state = State.NOTSTARTED;
    return this.timer;

  }

  sessionCountDown() {
    const $this = this;

      Observable
      .interval(1000) // timer(firstValueDelay, intervalBetweenValues)
      .map(i => this.start - i)
      .take(this.start + 1)
      .takeUntil(this.pause$)
      .subscribe(i => {
        this.timer.state = State.INPROGRESS;
        const seconds = Number(i);
        const minutes = this.timer.time.min.value - 1;

        if (seconds === 0 ) {
          this.notifyProgressEvent();
          if(minutes === -1) {
          this.onSessionComplete();
          } else {
            this.sessionCountDown();
          }
        }
        this.timer.time.setSeconds(seconds);

        if(seconds === 59){
          this.timer.time.setMinutes(minutes);
        }

      });

    return {
      resume() {
        $this.start = $this.timer.time.sec.value;
        $this.timer.state = State.INPROGRESS;
      },
      pause() {
        $this.pause$.next(true);
        $this.timer.state = State.PAUSED;
      }
    };
  }
  breakCountDown() {
    console.log(this.timer)
     const v = 59;
    Observable
      .interval(1000) // timer(firstValueDelay, intervalBetweenValues)
      .map(i => v - i)
      .take(v + 1)
      .subscribe(i => {
        this.timer.state = State.ONBREAK;
        const seconds = Number(i);
        const minutes = this.timer.time.min.value - 1;

        if (seconds === 0 ) {
          this.notifyProgressEvent();
          console.log('notify prog')
          if(minutes === -1) {
           // this.onSessionComplete();
          } else {
            this.breakCountDown();
          }
        }
        this.timer.time.setSeconds(seconds);

        if(seconds === 59){
          this.timer.time.setMinutes(minutes);
        }
      });


  }


  onSessionComplete() {
    this.notifyProgressEvent();
    this.playSessionEndSound();
    this.pause$.next(true);
   this.notifySessionEndEvent(this.timer.task);
  }


   reset() {

  }

  updateSessionTime(time: number) {
    if(!isUndefined(this.timer)) {

      this.timer.sessionTime = time;
      this.timer.time.setMinutes(time);
      this.notifyProgressEvent();
      return this.timer.sessionTime;
    }
    return 25;

  }

  killSession() {
    this.timer = null;
    this.pause$.next(true);
    this.notifyProgressEvent(true);
  }

  playSessionEndSound() {
    const sound = new Howl({
      src: ['../assets/sounds/alarm1.mp3'],
      html5 :true
    });
// Fires when the sound finishes playing.
    sound.on('end', function(){

    });


    sound.play();


  }

  /**
   * trigger event to progress change
   *
   */
  notifyProgressEvent(reset?: boolean) {

    if( (isBoolean(reset) && reset === true) ||  this.timer.time.min.value === 0) {
      this.progressEvent.next(0);
      return false;
    }

    const perc = ((this.timer.sessionTime - this.timer.time.min.value) / this.timer.sessionTime) * 100;
    console.log(perc)
    this.progressEvent.next(Math.round(perc));
  }

  /**
   * trigger event to begging timer
   *
   */
  notifyStartTimerEvent(task: Task) {
    this.startTimerEvent.next(task);
  }


  /**
   * trigger event to notify end of session
   *
   */
  notifySessionEndEvent(task: Task) {
    this.sessionEndEvent.next(task);
  }

}
