import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {State, Task, Timer} from "../timer.model";
import {TimerService} from "../timer.service";
import {AuthService} from "../auth.service";
import {isUndefined} from "util";

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss']
})
export class ClockComponent implements OnInit {
  timer: Timer;
  states = State;
  constructor(private timerSrv: TimerService, private elementRef: ElementRef, private render:  Renderer2, private _authSrv: AuthService) { }

  ngOnInit() {
    this.timerSrv.startTimerEventAnnounced$.subscribe( (task: Task) => {
      this.timer = this.timerSrv.init(task);
      this.timerSrv.sessionCountDown();
      this.timerSrv.progressEventAnnounced$.subscribe((val) => {
        const el = this.elementRef.nativeElement.querySelector('#PomodoroClock');
        this.render.setAttribute(el, 'data-progress', val.toString());
      });
      this.timerSrv.timer$.next(this.timer);
    });

    this._authSrv.logInOutEventAnnounced$.subscribe(state => {
      if (state === false) {
        this.timerSrv.killSession();
        this.timer = undefined;
      }
    });


    this.timerSrv.sessionEndEventAnnounced$.subscribe(() => {
      this.startBreak();
    });

  }

  startBreak() {
    const circleParent = this.elementRef.nativeElement.querySelector('#PomodoroClock');

    this.render.addClass(circleParent, 'break-timer');
    this.timer.sessionTime =5;
    this.timer.time.setMinutes(5)
    this.timerSrv.breakCountDown();
  }

  pauseResume() {
    const pauseResumeBtn = this.elementRef.nativeElement.querySelector('button.pause');
     console.log('state ', this.timer.state)
    if(this.timer.state === State.INPROGRESS){
       this.timerSrv.sessionCountDown().pause();
       pauseResumeBtn.textContent = 'Resume';
     } else if(this.timer.state === State.PAUSED) {
       this.timerSrv.sessionCountDown().resume();
      pauseResumeBtn.textContent = 'Pause';

     }
  }


  resetSession() {
    // this.timerSrv.notifyStartTimerEvent(this.timer.task.name);
  }





}
