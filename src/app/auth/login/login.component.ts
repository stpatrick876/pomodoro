import { Component, OnInit } from '@angular/core';
import {WelcomeComponent} from "../welcome.component";
import {AuthService} from "../../auth.service";
import {MdDialogRef} from "@angular/material";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginErr: boolean;
  constructor(private _authSrv: AuthService, public dialogRef: MdDialogRef<WelcomeComponent>) { }

  ngOnInit() {
  }
  login(){
    this._authSrv.googleLogin().then(res => {
      console.log('res', res)
      this.dialogRef.close();
    });
  }

  loginAnon(){
    this._authSrv.anonLogin().then(res => {
      this.dialogRef.close();
    }, err => {
      this.loginErr = true;

      setTimeout(() => {
        this.loginErr = false;
      }, 4000);
    });
  }
}
