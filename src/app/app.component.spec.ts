import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {TasksComponent} from "./tasks/tasks.component";
import {ClockComponent} from "./clock/clock.component";
import {SettingsComponent} from "./settings/settings.component";
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {MD_MODULES} from "./app.module";
import {TimerService} from "./timer.service";
import {AuthService} from "./auth.service";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireModule} from "angularfire2";
import {environment} from "../environments/environment";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {WelcomeComponent} from "./auth/welcome.component";
import {LoginComponent} from "./auth/login/login.component";
import {Observable} from "rxjs/Observable";
import {setTimeout} from "timers";

@NgModule({
  declarations: [WelcomeComponent, LoginComponent],
  schemas:[NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    WelcomeComponent, LoginComponent
  ],
  exports: [WelcomeComponent, LoginComponent]
})
class MockModule {}


fdescribe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TasksComponent,
        ClockComponent,
        SettingsComponent
      ],
      imports: [...MD_MODULES, AngularFireAuthModule, AngularFireModule.initializeApp(environment.firebase, 'pomodoro-app'),
        NoopAnimationsModule, MockModule],
      schemas:[NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [TimerService, AuthService],


    }).compileComponents();
  }));

  it('should create the app',() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should hide task pane when session starts ', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const _timerSrv = TestBed.get(TimerService);
     app.showtasks = true;
    _timerSrv.notifyStartTimerEvent();
    expect(app.showtasks).toBeFalsy();
  });

  it('should get user on init', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const _authrSrv = TestBed.get(AuthService);
     _authrSrv.user = Observable.of({id: '123'})
    expect(app.user).toBeUndefined();
    app.ngOnInit();
    expect(app.user).toBeDefined();
    expect(app.user.id).toEqual('123');
  });

  it('should load popup on first load ', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    spyOn(localStorage, 'getItem').and.returnValue('false');
    expect(app.welcomeDialogRef).toBeUndefined();

    app.ngOnInit();
    expect(app.welcomeDialogRef.componentInstance).toBeDefined();

  });

  fit('should hide task pane when user logs out ', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    const _authSrv = TestBed.get(AuthService);
    spyOn(_authSrv, 'logout').and.returnValue(new Promise((resolve => {
      resolve('');
    })));
    app.showtasks = true;
    app.logout();
    expect(app.showtasks).toBeFalsy();
  });




});
