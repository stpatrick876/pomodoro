import {browser, by, element} from "protractor";
import {Observable} from "rxjs/Observable";
import 'rxjs'
export class WelcomePage {

  locators: any = {
    stepOneTitle: by.xpath('//span[@class="title-focus"]')
  }


  navigateTo() {
    return browser.get('/');
  }


 verifyWelcomeDialog1(): Observable<boolean> {
    let retObs: Observable<boolean> = Observable.of(false);


   element(this.locators.stepOneTitle).getText().then((text) => {
     if( text === 'WHAT IS') {
       console.log('********************** ', text, ' ************* ')

       retObs = Observable.of(true);
     }

       });


    return retObs;

 }



}
