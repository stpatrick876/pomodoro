

import {WelcomePage} from "./welcome.po";
import {browser, by, element, protractor} from "protractor";

describe('welcome page ', () => {
  let page: WelcomePage;

  beforeEach(() => {
    page = new WelcomePage();
    page.navigateTo();
  });


  it('should verify welcome page step 1 title', () => {
    element(by.xpath('//span[@class="title-focus"]')).getText().then((text) => {
      expect(text).toBe('WHAT IS');
    });
  });

  it('should have back and next buttons ', () => {

    const myElement = element(by.css('.title-focus'));
    expect(myElement.isPresent()).toBeTruthy();
    /*element(by.id('prev-btn')).isPresent().then((isPresent) => {
      console.log('***************** ', isPresent, '******************');
      expect(isPresent).toBeTruthy();
    });*/

    // element(by.xpath('//div[@class="step-nav"]/button[@id="prev-btn"]')).isPresent().then((isPresent) => {
    //   console.log('***************** ', isPresent, '******************');
    //   expect(isPresent).toBeTruthy();
    // });

  });



});
